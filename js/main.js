jQuery(function($)
{
    $(".hamburger").click(function()
    {
        $(".navigation").toggleClass("open");
    })
});


$('.category__carousel.owl-carousel').owlCarousel({
  autoplay: true,
  autoplayHoverPause: true,
  loop: true,
  margin: 20,
  responsiveClass: true,
  nav: true,
  loop: true,
  responsive: {
    0: {
      items: 1
    },
    568: {
      items: 2
    },
    600: {
      items: 2
    },
    1000: {
      items: 3
    }
  }
})


  /*----------------------------------------
      Product Carousel Two
  ---------------------------------------- */
      $('.product-carousel-two.owl-carousel').owlCarousel({
          loop:true,
          items:4,
          margin:25,
          nav:true,
          navText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
          responsive:{
              0:{
                  items:1
              },
              420:{
                  items:2
              },
              600:{
                  items:4
              },
              800:{
                  items:4
              },
              1024:{
                  items:4
              },
              1200:{
                  items:4
              }
          }
      });










      $(document).ready(function() {
        $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
        $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
    });


      


    



    jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"><i class="fa fa-plus"></i></div><div class="quantity-button quantity-down"><i class="fa fa-minus"></i></div></div>').insertAfter('.shiping-cart__quantity input');
    jQuery('.shiping-cart__quantity').each(function() {
      var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });





    $(document).ready(function(){
      $('.btn-expand-collapse').click(function(){
          $child=$(this).children('span');
          $child.toggleClass("glyphicon glyphicon-menu-right").toggleClass("glyphicon glyphicon-menu-left");
      });
  });
  

  
  $(function() {
    $(".expand").on( "click", function() {
      // $(this).next().slideToggle(200);
      $expand = $(this).find(">:first-child");
      
     
    });
  });






      //Product Single Details
// Product Images Slider

function init() {
  var map = new google.maps.Map(document.getElementById('map-canvas'), {
    center: {
      lat: 30.049039,
      lng: 31.209579
    },
    zoom: 12
  });


  var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('pac-input'));
  google.maps.event.addListener(searchBox, 'places_changed', function() {
    searchBox.set('map', null);


    var places = searchBox.getPlaces();

    var bounds = new google.maps.LatLngBounds();
    var i, place;
    for (i = 0; place = places[i]; i++) {
      (function(place) {
        var marker = new google.maps.Marker({

          position: place.geometry.location
        });
        marker.bindTo('map', searchBox, 'map');
        google.maps.event.addListener(marker, 'map_changed', function() {
          if (!this.getMap()) {
            this.unbindAll();
          }
        });
        bounds.extend(place.geometry.location);


      }(place));

    }
    map.fitBounds(bounds);
    searchBox.set('map', map);
    map.setZoom(Math.min(map.getZoom(),12));

  });
}
google.maps.event.addDomListener(window, 'load', init);


